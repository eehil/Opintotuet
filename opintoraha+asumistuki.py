import numpy as np
import matplotlib.pyplot as plt

def vuodenlopun_tulot(pvpervko, tuntipalkka):
    pva = pvpervko*4*4
    tunnit = pva * 7.5
    brutto_palkka = tunnit*tuntipalkka
    return brutto_palkka

def opintoraha_lisays(tulot,tyyppi):
    opintoraha_brutto = 250.28
    opintoraha_netto = 225.26
    tukikuukaudet = 0
    for i in range(13):
        if tulot < (8004 + 667*i):
            tukikuukaudet = 12 - i
            #print("Tukikuukaudet", tukikuukaudet)
            break
    if tyyppi == "brutto":
        return tukikuukaudet * opintoraha_brutto
    else:
        return tukikuukaudet * opintoraha_netto
        

def main():
    lahto_tulot = 0
    x = []
    y = []
    y2 = []
    while lahto_tulot < 20000:
        brutto_tulot = lahto_tulot
        tuntipalkka = 11.68
        pvapervko = 2
        
        brutto_tulot += vuodenlopun_tulot(pvapervko,tuntipalkka)
        netto_tulot = brutto_tulot
        
        
        netto_tulot += opintoraha_lisays(brutto_tulot, "brutto")
        brutto_tulot += opintoraha_lisays(brutto_tulot, "netto")
        
        #asumistuki
        vuokra = 386
        aikuiset = 1
        lapset = 0
        y2.append(netto_tulot)
        keskiarvotulo = (brutto_tulot / 12) - 300
        
        perusomavastuu = 0.42 * (keskiarvotulo - (597 + (99 * aikuiset) + (221 * lapset)))
        if perusomavastuu < 0:
            perusomavastuu = 0
        asumistuki = 0.8 *(vuokra - perusomavastuu)
        
        netto_tulot += asumistuki*4
        x.append(lahto_tulot)
        y.append(netto_tulot)
        #print(netto_tulot)
        #print(asumistuki)
        lahto_tulot += 10
    plt.plot(x,y,x,y2)
    plt.show()
    
    
    
main()